<?php

use App\Models\Role;
use App\Models\User;
use App\Models\PostCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Création des rôles
         */
        Role::firstOrCreate(
            [
                'id' => env('ADMIN_ROLE_ID')
            ],
            [
                'id' => env('ADMIN_ROLE_ID'),
                'name' => 'admin',
                'label' => 'administrateur'
            ]
        );

        Role::firstOrCreate(
            [
                'name' => 'user'
            ],
            [
                'name' => 'user',
                'label' => 'utilisateur'
            ]
        );

        Role::firstOrCreate(
            [
                'name' => 'vente'
            ],
            [
                'name' => 'vente',
                'label' => 'vendeur'
            ]
        );

        Role::firstOrCreate(
            [
                'name' => 'elevage'
            ],
            [
                'name' => 'elevage',
                'label' => 'éleveur'
            ]
        );

        /**
         * Création du compte admin
         */
        $admin = User::firstOrCreate(
            [
                'id' => env('ADMIN_ID')
            ],
            [
                'name' => env('ADMIN_NAME'),
                'email' => env('ADMIN_ADRESS'),
                'password' => Hash::make(env('ADMIN_PASSWORD')),
            ]
        );
        $admin->roles()->attach(env('ADMIN_ROLE_ID'));

        /**
         * Création des catégories
         */
        PostCategory::firstOrCreate(
            [
                'name' => 'vente'
            ],
            [
                'name' => 'vente'
            ]
        );

        PostCategory::firstOrCreate(
            [
                'name' => 'elevage'
            ],
            [
                'name' => 'elevage'
            ]
        );
    }
}
