<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Integer;

class ProfilController extends Controller
{
    public function show()
    {

        if (Auth::check()) {
            $user = User::findOrFail(Auth::user()->id);
            $roles = Role::where('name', '!=', 'user')->where('name', '!=', 'admin')->get();
            return view('layouts.profil.profil', compact('user', 'roles'));
        }
        else {
            return view('auth.login');
        }
    }

    public function deleteUser(Request $request) {
        $user = User::findOrFail($request->get('id'));
        $user->roles()->detach();
        $user->delete();
        return redirect()->route('/');
    }

    public function updateUserRole(Request $request) {
        $user = User::findOrFail($request->get('id'));
        $userRoles = collect(Role::where('name', 'user')->first()->id);
        if ($user->roles()->pluck('id')->first() === 1)
        {
            $userRoles->push(1);
        }
        if ($request->get('elevage'))
        {
            $userRoles->push($request->get('elevage'));
        }
        if ($request->get('vente'))
        {
            $userRoles->push($request->get('vente'));
        }
        $user->roles()->sync($userRoles);
        return redirect(route('profil.show', $user->id));

    }

    public function updateUserData(Request $request) {
        $user = User::findOrFail($request->get('id'));
        $name = $request->get('name');
        $email = $request->get('email');
        $user->update([
            'name' => $name,
            'email' => $email,
            'updated_at' => now(),
        ]);

        return redirect(route('profil.show', $user->id));
    }
}
