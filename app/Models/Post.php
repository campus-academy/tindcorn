<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'description',
        'image',
        'price',
        'category_id',
        'seller_id',
        'buyer_id'
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'seller_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'category_id', 'id');
    }
}
