@extends('layouts.app')

@section('content')
<div class="container">
    <div style="padding-left: 2em; background-image: url('../img/525809.jpg'); background-size: cover; height: 25em; display: flex; align-items: center" class="">
        <img src="../img/unknown.png"  style="border-radius: 50%; width: 13rem; height: 13rem; border: 4px solid #fff"  class="" alt="default pp">
    </div>
    <div class="" style="margin-top: 1em">
        <h1 class="card-title">Bonjour {{$user->name}}</h1>
        <p class="text-justify" style="font-size: 16px; color: #1b1e21">Bienvenue sur votre espace personnel, vous pouvez demander de nouveau role, ou encore modifier vos préférences.</p>
        <div class="" style="display: flex; flex-direction: row">
            <nav class="nav nav-pills flex-column" style="max-width: 15rem; margin-right: 5rem">
                <a class="nav-item nav-link active" href="#roleSection" data-toggle="tab">Demande de rôle</a>
                <a class="nav-item nav-link" href="#preferences" data-toggle="tab">Préférences</a>
                <a class="nav-item nav-link" href="#deleteAccount" data-toggle="tab">Suppression du compte</a>
            </nav>

            <div class="tab-content">
                <div id="roleSection" class="tab-pane active">
                    <h1 class="card-title">Demande de rôle</h1>
                    <h5>Sélectionner les rôles que vous-effectuez</h5>
                    <form action="{{ route('profil.updateUserRole') }}" method="POST">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        @foreach($roles as $role)
                            <div class="form-check">
                                <input class="form-check-input" {{$user->roles()->pluck('id')->contains($role->id) ? 'checked' : ''}} type="checkbox" id="gridCheck{{$role->id}}" value="{{$role->id}}" name="{{$role->name}}">
                                <label class="form-check-label" for="gridCheck{{$role->id}}">
                                    Je suis {{$role->name}}
                                </label>
                            </div>
                        @endforeach
                        <button type="submit" class="btn btn-outline-success">Valider</button>
                    </form>
                </div>

                <div id="preferences" class="tab-pane" style="">
                    <h1 class="card-title">Préferences</h1>
                    <form style="display: flex; flex-direction: column" method="POST" action="{{route('profil.updateUserData')}}">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        <label for="name">Votre nom complet</label>
                        <input value="{{$user->name}}" id="name" name="name">
                        <label for="email">Votre email</label>
                        <input value="{{$user->email}}" id="email" name="email">
                        <h5 style="margin-top: 1em">Date de création du compte : <span class="font-italic">{{$user->created_at}}</span></h5>
                        <h5 style="margin-top: 1em">Dernière modification du compte : <span class="font-italic">{{$user->updated_at}}</span></h5>
                        <button class="btn btn-outline-success" type="submit">Valider</button>
                    </form>
                </div>

                <div class="tab-pane" id="deleteAccount">
                    <h1 class="card-title">Suppression du compte</h1>
                    <h5>Voulez-vous supprimer votre compte ?</h5>
                    <form action="{{ route('profil.deleteUser') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        <button type="submit" class="btn btn-outline-danger" onclick="{{route('profil.deleteUser', $user->id)}}">Oui</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection

