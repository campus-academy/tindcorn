@extends('home')

@section('container')
    <link rel="stylesheet" type="text/css"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/dt-1.10.21/af-2.3.5/b-1.6.2/cr-1.5.2/fc-3.3.1/fh-3.1.7/kt-2.5.2/r-2.2.5/rg-1.1.2/rr-1.2.7/sc-2.0.2/sp-1.1.1/sl-1.3.1/datatables.min.css"/>

    <table class="table table-bordered table-striped table-hover" id="datatable">
        <thead>
        <tr>
            <th>Voir</th>
            <th>Titre</th>
            <th>Description</th>
            <th>Vendeur</th>
            <th>Type</th>
            <th>Prix</th>
            <th>Ajouté le</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@endSection

@section('script')
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


    <script type="text/javascript"
            src="https://cdn.datatables.net/v/bs4/dt-1.10.21/af-2.3.5/b-1.6.2/cr-1.5.2/fc-3.3.1/fh-3.1.7/kt-2.5.2/r-2.2.5/rg-1.1.2/rr-1.2.7/sc-2.0.2/sp-1.1.1/sl-1.3.1/datatables.min.js"></script>

    <script type="text/javascript" src="https://momentjs.com/downloads/moment-with-locales.min.js"></script>

    <script>
        moment.lang("fr");
        let table = $('#datatable').DataTable({
            "ajax": {
                url: './posts/getList',
                type: "GET",
                dataType: 'json',
                dataSrc: ""
            },
            "serverSide": false,
            "searching": true,
            "ordering": true,
            "columns": [
                {"data": null},
                {"data": "title"},
                {"data": "description"},
                {"data": "user.name"},
                {"data": "category.name"},
                {"data": "price"},
                {"data": "created_at"}
            ],
            "columnDefs": [
                {
                    "targets": 0,
                    "orderable": false,
                    "searchable": false,
                    "render": function (data, type, row, meta) {
                        return ('<div class="text-primary edit" data-description="' + row["description"] + '" data-target="' + row["id"] + '"><a href="./posts/' + row["id"] + '"><i class="fa fa-eye fa-lg" ></i></a></div>');
                    }
                },
                {
                    "targets": 5,
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return ('<p>' + row["price"] + ' €</p>');
                    }
                },
                {
                    "targets": 6,
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        if (data && data != '') {
                            return moment(data).format('lll');
                        }
                    }
                }
            ],
            pageLength: 25,
            drawCallback: function (settings) {
            },
            success: function () {

            }
        });
    </script>
@endsection
