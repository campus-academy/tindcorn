@extends('home')

@section('container')
    <div class="card">
        <div class="card-header"><h3>Création d'une annonce</h3></div>
        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Titre</label>
                    <input type="text" required class="form-control" id="title" name="title" placeholder="Titre">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea required class="form-control" id="description" name="description" placeholder="Description"></textarea>
                </div>
                <div class="form-group">
                    <label for="price">Prix</label>
                    <input type="number" required class="form-control" id="price" name="price" placeholder="Prix">
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="text" class="form-control" id="image" name="image" placeholder="Lien de l'image">
                </div>
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-12 pt-0">Catégorie de l'annonce:</legend>
                        <div class="col-sm-10">
                            @foreach ($categories as $category)
                                @if (Auth::user()->roles->pluck('name')->contains($category->name) || Auth::user()->roles->pluck('name')->contains(env('ADMIN_NAME')))
                                    <div class="form-check">
                                    <input class="form-check-input" type="radio" name="category" id="{{ $category->name }}" value="{{ $category->id }}" checked>
                                        <label class="form-check-label" for="{{ $category->name }}">
                                            {{ $category->name }}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-primary">Créer</button>
            </form>
        </div>
    </div>
@endsection