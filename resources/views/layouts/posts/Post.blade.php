@extends('home')

@section('container')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width: 30rem;">
                @if (filter_var($post->image, FILTER_VALIDATE_URL))
                    <img class="card-img-top" src="{{ $post->image }}" alt="Card image cap">
                @endif
                <div class="card-body">
                    <h5 class="card-title">{{ $post->title }}</h5>
                    <p class="card-text">{{ $post->description }}</p>
                    
                    @if (Auth::check())                           
                        <p>
                            <a id="checkout-button" href="{{ route('payment.form', $post->id) }}" class="btn btn-success">Acheter</a> {{ $post->price }} €
                            @if (Auth::user()->id == $post->seller_id)
                                <form action="{{route('posts.destroy', $post->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger float-right">Supprimer</button>
                                </form>
                                {{-- <a href="#" class="btn btn-danger float-right">Supprimer</a> --}}
                                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary float-right mr-3">Modifier</a>   
                            @endif
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection