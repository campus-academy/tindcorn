<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Tindcorn') }} - Home</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                overflow: hidden;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="max-height: 100vh">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="img/logo.svg"  style="width: 3rem; height: auto; max-width: 200px; max-height: 200px; margin-right: 1rem" alt="logo"/>
                {{ config('app.name', 'Tindcorn') }}
            </a>
            <ul class="navbar-nav ml-auto">
                @if (Route::has('login'))
                    <div class="nav-item links">
                        @auth
                            <a href="{{ route('posts.index') }}">Home</a>
                        @else
                            <a href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif
            </ul>

        </div>
    </nav>
        <div class="flex-center position-ref full-height" style="background-color: rgba(0, 0, 0, 0.3); background-image: url('img/unicorn-bg.jpg'); background-size: cover;">
            <div class="content card" style="padding: 2em" >
                <div class="title m-b-md">
                    {{ config('app.name', 'Tindcorn') }}
                </div>

                <div class="links">
                    <a onclick="alert('Mais non tu ne veux quand même pas regarder ça :)')">licornhub.com</a>
                    <a href="https://gitlab.com/campus-academy/tindcorn" target="_blank">GitLab</a>
                    <a href="{{ route('posts.index') }}">Annonces</a>
                </div>
            </div>
        </div>
    </body>
</html>
