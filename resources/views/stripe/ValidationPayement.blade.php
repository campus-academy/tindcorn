@extends('home')

@section('container')
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width: 30rem;">
                <p>Merci {{ $user->name }} pour le paiement de {{ $post->price }} €.</p>
            </div>
        </div>
    </div>
@endsection