<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('/');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('roles','RoleController')->except('show');
Route::get('posts/getList', 'PostController@getList')->name('posts.getList');
Route::resource('posts', 'PostController');

Route::get('/profil/{id}', 'ProfilController@show')->name('profil.show');
Route::delete('/profil', 'ProfilController@deleteUser')->name('profil.deleteUser');
Route::post('/profil', 'ProfilController@updateUserRole')->name('profil.updateUserRole');
Route::post('/profil/update-data', 'ProfilController@updateUserData')->name('profil.updateUserData');

Route::get('/paiement/{post}', 'PaiementController@formulaire')->name('payment.form');
Route::get('/paiement-ok/{post}', 'PaiementController@paiementOK')->name('payment.ok');
